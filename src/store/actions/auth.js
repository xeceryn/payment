import AsyncStorage from '@react-native-community/async-storage';

import { AUTH_SET_TOKEN, TRY_AUTH, AUTH_REMOVE_TOKEN } from './actionTypes';
import {uiStartLoading, uiStopLoading} from './index';
import startMainTabs from '../../screens/MainTabs/startMainTabs';
import App from '../../../App';

const API_KEY = "AIzaSyAzit4eKeCd2ccuZh4VSzTIXy4zlz-4wM0";

export const tryAuth = (authData, authMode) => {
    return dispatch => {
        let url = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=" + API_KEY
        dispatch(uiStartLoading());
        if (authMode === "signup") {
            url = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=" + API_KEY
        } 
        fetch(url, {
            method: "POST",
            body: JSON.stringify({
                email: authData.email,
                password: authData.password,
                returnSecureToken: true
            }),
            headers: {
                "Content-Type": "application/json"
            }
        })
        .catch(err => {
            console.log(err);
            alert("Something wrong, Please try again");
            dispatch(uiStopLoading());
        })
        .then(res => res.json())
        .then(parsedRes => {
            console.log(parsedRes);
            dispatch(uiStopLoading());
            if (!parsedRes.idToken){
                alert("Something wrong, Please try again");
            }else {
                dispatch(
                    authStoreToken(
                        parsedRes.idToken, 
                        parsedRes.expiresIn, 
                        parsedRes.refreshToken
                    )
                );
                startMainTabs();
            }
        });
    };
};

export const authStoreToken = (token, expiresIn, refreshToken) => {
    return dispatch => {
        const now = new Date();
        const expiryDate = now.getTime() + expiresIn * 1000;
        dispatch(authSetToken(token, expiryDate));
        AsyncStorage.setItem("token", token);
        AsyncStorage.setItem("expiryDate", expiryDate.toString());
        AsyncStorage.setItem("refreshToken", refreshToken);
    }
}

export const authSetToken = (token, expiryDate) => {
    return {
        type: AUTH_SET_TOKEN,
        token: token,
        expiryDate: expiryDate
    };
};

export const authGetToken = () => {
    return (dispatch, getState) => {
        const promise = new Promise((resolve, reject) => {
            const token = getState().auth.token;
            const expiryDate = getState().auth.expiryDate;
            let fetchedToken;
            if (!token || new Date(expiryDate) <= new Date()){
                AsyncStorage.getItem("token")
                .catch(err => reject())
                .then(tokenFromStorage => {
                    fetchedToken = tokenFromStorage;
                    if(!tokenFromStorage){
                        reject();
                        return;
                    }
                    return AsyncStorage.getItem("expiryDate");
                })
                .then(expiryDate => {
                    const parsedExpiryDate = new Date(parseInt(expiryDate));
                    const now = new Date();
                    if (parsedExpiryDate > now){
                        dispatch(authSetToken(fetchedToken));
                        resolve(fetchedToken);
                    } else {
                        reject();
                    }
                    
                })
                .catch(err => reject());
            } else {
                resolve(token);
            }
        });
        return promise
        .catch(err => {
            return AsyncStorage.getItem("refreshToken")
            .then(refreshToken => {
                return fetch("https://securetoken.googleapis.com/v1/token?key=" + API_KEY, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    },
                    body: "grant_type=refresh_token&refresh_token=" + refreshToken
                });
            })
            .then(res => res.json())
            .then(parsedRes => {
                if(parsedRes.id_token){
                    console.log("Refresh token OK");
                    dispatch(
                        authStoreToken(
                            parsedRes.id_token, 
                            parsedRes.expires_in, 
                            parsedRes.refresh_token
                        )
                    );
                    return parsedRes.id_token;
                }else {
                    dispatch(authClearStorage());
                }
            });
        })
        .then(token => {
            if (!token){
                throw(new Error());
            }else {
                return token;
            }
        });
    };
};

export const authAutoSignin = () => {
    return dispatch => {
        dispatch(authGetToken())
        .then(token =>{
            startMainTabs();
        })
        .catch(err => console.log("Failed to get Token"));
    }
}

export const authClearStorage = () => {
    return dispatch => {
        AsyncStorage.removeItem("token");
        AsyncStorage.removeItem("expiryDate");
        return AsyncStorage.removeItem("refreshToken");
    }
}

export const authLogout = () => {
    return dispatch => {
        dispatch(authClearStorage())
        .then(() => {
            App();
        });  
        dispatch(authRemoveToken());
    }
}

export const authRemoveToken = () => {
    return {
        type: AUTH_REMOVE_TOKEN
    };
};