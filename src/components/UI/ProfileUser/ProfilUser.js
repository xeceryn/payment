import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default class ProfileUser extends Component {
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.top}>
                    <View style={styles.profilImage}></View>
                </View>
                <View style={styles.center}></View>
                <View style={styles.divider}></View>
                <View style={styles.center}></View>
                <View style={styles.divider}></View>
                <View style={styles.center}></View>
                <View style={styles.divider}></View>
                <View style={styles.center}></View>
                <View style={styles.divider}></View>
                <View style={styles.center}></View>
                <View style={styles.divider}></View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    top: {
        height: '45%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'orange'
    },
    profilImage: {
        width: 140,
        height: 140,
        borderRadius: 100,
        borderWidth: 4,
        borderColor: '#fff',
        backgroundColor: '#eee'
    }, 
    center: {
        height: '10%',
        backgroundColor: '#eee'
    },
    divider: {
        height: '1%',
        backgroundColor: '#fff'
    }
});