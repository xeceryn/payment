import React, {Component} from 'react';
import {View, Platform} from 'react-native';
import {Container, Content, Form, Item, Input, Label, Button, Text} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';

export default class TransferPayment extends Component {

    render(){
        return (
            <Container>
                <Content>
                    <Form>
                        <View style={{flexDirection: 'row'}}>
                            <Item floatingLabel style={{marginTop: 2, width: '75%'}}>
                                <Label>Masukkan Tujuan</Label>
                                <Input />
                            </Item>
                            <View style={{justifyContent: 'flex-end', alignItems: 'flex-end', marginLeft: 5}}>
                                <Icon name={Platform.OS === 'android' ? 'md-contacts' : 'ios-contacts'} size={30} color='black' />
                            </View>
                            <View style={{justifyContent: 'flex-end', alignItems: 'flex-end', marginLeft: 10}}>
                                <Icon name={Platform.OS === 'android' ? 'md-qr-scanner' : 'ios-qr-scanner'} size={30} color='black' />
                            </View>
                        </View>
                        <Item floatingLabel style={{marginTop: 2}}>
                            <Label>Masukkan Nominal Transfer</Label>
                            <Input />
                        </Item>
                            <Text style={{color: 'red', fontSize: 12, marginLeft: 10, marginTop: 2}}>Minimal transfer saldo Rp. 10.000</Text>
                        <Item floatingLabel style={{marginTop: 0}}>
                            <Label>Masukkan PIN</Label>
                            <Input />
                        </Item>
                    </Form>
                    <Button block success style={{marginHorizontal: 10, marginTop: 20}}>
                        <Text>TRANSFER</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}