import React, {Component} from 'react';
import {View, Text, StyleSheet, Platform, Image} from 'react-native';
import {Avatar} from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';

export default class OtuIdPayment extends Component {
    render(){
        return(
            <View style={styles.container}>
                <View style={{height: '15%', marginHorizontal: 5, marginTop: 5}}>
                    <View style={styles.topItemInner}>
                        <View style={{alignItems: 'center', flexDirection: 'row', flex: 2, marginLeft: 10}}>
                            <Avatar 
                                rounded
                                title="OTU"
                                activeOpacity={0.7}
                                size="medium"
                            />
                            <View style={{flexDirection: 'column', marginLeft: 10}}>
                                <Text style={{fontWeight: 'bold', fontSize: 16}}>OTU ID</Text>
                                <Text>+62 803489583940</Text>
                                <Text>EKL 00000000</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{height: '5%', backgroundColor: '#eee'}}>
                </View>
                <View style={{height: '70%', marginHorizontal: 5,}}>
                    <View style={styles.topItemInner}>
                        <View style={{flexDirection: 'row', padding: 7, justifyContent: 'space-between'}}>
                            <Text style={{fontWeight: 'bold', fontSize: 16}}>QR Code</Text>
                            <Icon name={Platform.OS === 'android' ? 'md-information-circle' : 'ios-information-circle'} size={30} color='green'/>
                        </View>
                        <View style={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image 
                                source={require('../../assets/frame.png')}
                                style={{height: 300, width: 300}} />
                        </View>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginRight: 5}}>
                            <Text></Text>
                            <Icon name={Platform.OS === 'android' ? 'md-download' : 'ios-download'} size={30} color='green'/>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }, 
    topItemInner: {
        flex: 1,
        backgroundColor: '#fff',
        shadowColor: '#000',
        borderRadius: 3,
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2
    }
});