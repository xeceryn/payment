import {Navigation} from 'react-native-navigation';
import {Platform} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const startTabs = () => {
    Promise.all([
        Icon.getImageSource(Platform.OS === 'android' ? "md-map" : "ios-map", 30),
        Icon.getImageSource(Platform.OS === 'android' ? "md-share-alt" : "ios-share", 30),
        Icon.getImageSource(Platform.OS === 'android' ? "md-menu" : "ios-menu", 30),
        Icon.getImageSource(Platform.OS === 'android' ? "md-cart" : "ios-cart", 30),
        Icon.getImageSource(Platform.OS === 'android' ? "md-construct" : "ios-construct", 30)
    ]).then(sources => {
        Navigation.startTabBasedApp({
            tabs: [
                {
                    screen: "tes.FindPlaceScreen",
                    label: "Find Place",
                    title: "Find Place",
                    icon: sources[0],
                    navigatorButtons: {
                        leftButtons: [
                            {
                                icon: sources[2],
                                title: "Menu",
                                id: "sideDrawerToggle"
                            }
                        ]
                    }
                },
                {
                    screen: "tes.SharePlaceScreen",
                    label: "Share Place",
                    title: "Share Place",
                    icon: sources[1],
                    navigatorButtons: {
                        leftButtons: [
                            {
                                icon: sources[2],
                                title: "Menu",
                                id: "sideDrawerToggle"
                            }
                        ]
                    }
                },
                {
                    screen: "tes.PaymentScreen",
                    label: "Payment",
                    title: "Payment",
                    icon: sources[3],
                    navigatorButtons: {
                        leftButtons: [
                            {
                                icon: sources[2],
                                title: "Menu",
                                id: "sideDrawerToggle"
                            }
                        ]
                    }
                },
                {
                    screen: "tes.TestingScreen",
                    label: "Testing",
                    title: "Testing",
                    icon: sources[4],
                }
            ],
            tabsStyle: {
                tabBarSelectedButtonColor: "orange"
            },
            appStyle: {
                tabBarSelectedButtonColor: "orange"
            },
            drawer: {
                left: {
                    screen: "tes.SideDrawer"
                }
            },
            stack: {
                children: [
                    {
                        screen: "tes.TransferPaymentScreen",
                    },
                    {
                        screen: "tes.OtuIdPaymentScreen",
                    },
                    {
                        screen: "tes.RiwayatPaymentScreen"
                    },
                    {
                        screen: "tes.TopUpPaymentScreen"
                    }
                ],
                options: {
                    tabs: {
                        visible: false,
                        drawBehind: true,
                        animate: false
                    }
                }
            },
            
        });
    });
};

export default startTabs;
