import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {ListItem} from 'react-native-elements';
import {Form, Item, Input, Label} from 'native-base';

class TopUpPayment extends Component{
    render(){
        return(
            <View style={styles.container}>
                <Text style={{color: 'green', backgroundColor: '#eee'}}>Isi ulang saldo OTU</Text>
                <Item floatingLabel >
                    <Label style={{color: 'green'}}>Masukkan Nominal</Label>
                    <Input />
                </Item>
                <Text style={{color: 'red', fontSize: 10}}>Minimal nominal isi ulang adalah Rp. 10.000</Text>
                <View style={{backgroundColor: '#eee'}}>
                    <Text style={{ marginTop: 10}}>Metode Pembayaran</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 5,
        marginVertical: 5
    }
});

export default TopUpPayment;