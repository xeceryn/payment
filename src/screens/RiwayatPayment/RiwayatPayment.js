import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {ListItem} from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
class RiwayatPayment extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.itemInner}>
                {
                    list.map((item, i) => (
                    <ListItem
                        key={i}
                        title={item.title}
                        leftIcon={{ name: item.icon }}
                        bottomDivider
                    />
                    ))
                }
                </View>
            </View>
        );
    }
}

const list = [
    {
        title: 'Riwayat Saldo',
        icon: 'av-timer'
    },
    {
        title: 'Riwayat Transaksi',
        icon: 'av-timer'
    },
    {
        title: 'Riwayat Deposit',
        icon: 'av-timer'
    },
    {
        title: 'Riwayat Penarikan',
        icon: 'av-timer'
    },
    {
        title: 'Riwayat Bonus',
        icon: 'av-timer'
    },
    {
        title: 'Riwayat Transfer Antar Member',
        icon: 'av-timer'
    }
];
  

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 5,
        marginVertical: 5
    },
    itemInner: {
        flex: 1,
        backgroundColor: '#fff',
        shadowColor: '#000',
        borderRadius: 3,
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2
    }
});

export default RiwayatPayment;