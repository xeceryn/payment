import React, {Component} from 'react';
import {StyleSheet, View, Text, Platform, ScrollView, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Slideshow from 'react-native-image-slider-show';

class Payment extends Component{
    static navigatorStyle = {
        navBarButtonColor: "orange"
    }

    constructor(props){
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);

        this.state = {
            position: 1,
            interval: null,
            dataSource: [
                {
                    url: "https://crm.eklanku.com/assets/imgMAX/main_banner/1.jpg",
                }, {
                    url: "https://crm.eklanku.com/assets/imgMAX/main_banner/2.jpg",
                }, {
                    url: "https://crm.eklanku.com/assets/imgMAX/main_banner/3.jpg",
                }, {
                    url: "https://crm.eklanku.com/assets/imgMAX/main_banner/4.jpg",
                }, {
                    url: "https://crm.eklanku.com/assets/imgMAX/main_banner/5.jpg",
                }, {
                    url: "https://crm.eklanku.com/assets/imgMAX/main_banner/6.jpg",
                }, {
                    url: "https://crm.eklanku.com/assets/imgMAX/main_banner/7.jpg",
                },
            ],
        }
    }

    onNavigatorEvent = event => {
        if (event.type === "NavBarButtonPress"){
            if (event.id === "sideDrawerToggle"){
                this.props.navigator.toggleDrawer({
                    side: "left",
                })
            }
        }
    }

    componentWillMount(){
        this.setState({
            interval: setInterval(() => {
              this.setState({
                position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
              });
            }, 5000)
        });
    }

    componentWillUnmount(){
        clearInterval(this.state.interval);
    }

    render(){
        return(
            <ScrollView style={styles.container}>
                <View style={{marginHorizontal: 3, marginTop: 5}}>
                    {/* top menu */}
                    <View style={ {marginBottom: 5}}>
                        <View style={{backgroundColor: 'green', flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 3, marginTop: 5, borderTopLeftRadius: 4, borderTopRightRadius: 4, padding: 10}}>
                            <View style={{alignItems:'center', width: '25%', flex: 1}}>
                                <TouchableOpacity 
                                    style={{alignItems: 'center'}} 
                                    // onPress={() => {
                                    //     this.props.navigator.push({
                                    //         screen: 'tes.TransferPaymentScreen'
                                    //     })
                                    // }}
                                    >
                                    <Icon name={Platform.OS === 'android' ? 'md-qr-scanner' : 'ios-qr-scanner'} size={30} color='white'/>
                                    <View style={styles.topTextUI}>
                                        <Text style={styles.topText}>BAYAR</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{alignItems:'center', width: '25%', flex: 1}}>
                                <TouchableOpacity 
                                    style={{alignItems: 'center'}} 
                                    onPress={() => {
                                        this.props.navigator.push({
                                            screen: "tes.TransferPaymentScreen",
                                            title: "Transfer"
                                        })
                                    }}>
                                    <Icon name={Platform.OS === 'android' ? 'md-pulse' : 'ios-pulse'} size={30} color='white'/>
                                    <View style={styles.topTextUI}>
                                        <Text style={styles.topText}>TRANSFER</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{alignItems:'center', width: '25%', flex: 1}}>
                                <TouchableOpacity
                                    style={{alignItems: 'center'}} 
                                    onPress={() => {
                                        this.props.navigator.push({
                                            screen: "tes.OtuIdPaymentScreen",
                                            title: "OTU ID"
                                        })
                                    }}
                                    >
                                    <Icon name={Platform.OS === 'android' ? 'md-clipboard' : 'ios-clipboard'} size={30} color='white'/>
                                    <View style={styles.topTextUI}>
                                        <Text style={styles.topText}>OTU ID</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{alignItems:'center', width: '25%', flex: 1}}>
                                <TouchableOpacity
                                    style={{alignItems: 'center'}} 
                                    onPress={() => {
                                        this.props.navigator.push({
                                            screen: "tes.RiwayatPaymentScreen",
                                            title: "Riwayat"
                                        })
                                    }}
                                    >
                                    <Icon name={Platform.OS === 'android' ? 'md-paper' : 'ios-paper'} size={30} color='white'/>
                                    <View style={styles.topTextUI}>
                                        <Text style={styles.topText}>RIWAYAT</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{backgroundColor: '#32CD32', flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 3, padding: 5, borderBottomLeftRadius: 4, borderBottomRightRadius: 4}}>
                            <View >
                                <View style={{flexDirection: 'row'}}>
                                    <Text style={{color: 'white'}}>Bonus</Text>
                                    <Icon name='logo-bitcoin' size={15} color='white'/>
                                </View>
                                <View>
                                    <Text style={styles.topText}>Rp. 1000000</Text>
                                </View>
                            </View>
                            <View >
                                <View style={{flexDirection: 'row'}}>
                                    <Text style={{color: 'white'}}>Saldo</Text>
                                    <Icon name={Platform.OS === 'android' ? 'md-cash' : 'ios-cash'} size={15} color='white'/>
                                </View>
                                <View>
                                    <Text style={styles.topText}>Rp. 1000000</Text>
                                </View>
                            </View>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.navigator.push({
                                        screen: "tes.TopUpPaymentScreen",
                                        title: "Top Up"
                                    })
                                }}
                            >
                                <View >
                                    <Text style={{backgroundColor: '#FFD700', color: 'green', padding: 10, fontWeight: 'bold'}}>TOP UP</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>

                    {/* slider banner */}
                    <View style={{marginHorizontal: 3}}>
                            <Slideshow
                                height={130}
                                dataSource={this.state.dataSource}
                                position={this.state.position}
                                onPositionChanged={position => this.setState({ position })}
                                arrowSize={0}
                            />
                    </View>

                </View>

                {/* center text */}
                <View style={{paddingTop: 5}}>
                    <Text style={{color: 'green', paddingLeft: 10, fontWeight: 'bold'}}>Pilih tombol sesuai kebutuhan Anda :</Text>
                </View>

                {/* bottom menu */}
                <View style={{marginHorizontal: 7, marginBottom: 10}}>
                    <View >
                        <View style={[styles.bottomItemInner, {marginTop: 10}]}>
                            <View style={{paddingLeft: 15, paddingTop: 5}}>
                               <Text style={{fontWeight: 'bold', fontSize: 13, color: 'black'}}>ISI ULANG</Text>
                               <Text style={{fontSize: 10}}>Makin banyak transaksi, makin banyak untungnya!</Text>
                            </View>
                            <View style={styles.topIcon}>
                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>Pulsa</Text>
                                </View>

                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>Paket Data</Text>
                                </View>

                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>SMS</Text>
                                </View>

                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>Telephone</Text>
                                </View>
                           </View>
                           <View style={[styles.topIcon]}>
                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>Token Listrik</Text>
                                </View>

                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>E-Toll</Text>
                                </View>

                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>E-Saldo</Text>
                                </View>

                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>Wifi</Text>
                                </View>
                           </View>
                           <View style={[styles.topIcon]}>
                                <View style={{alignItems: 'center', width: '25%', marginBottom: 7 }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name='logo-game-controller-b' size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>Voucher Game</Text>
                                </View>
                           </View>
                       </View>
                       {/* bottom seconds item */}
                       <View style={[styles.bottomItemInner, {marginTop: 10}]}>
                            <View style={{paddingLeft: 15, paddingTop: 5}}>
                               <Text style={{fontWeight: 'bold', fontSize: 13, color: 'black'}}>TAGIHAN</Text>
                               <Text style={{fontSize: 10}}>Bayar tagihan ga pake ribet!</Text>
                            </View>
                            <View style={styles.topIcon}>
                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>Tagihan Listrik</Text>
                                </View>

                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>BPJS</Text>
                                </View>

                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>Telkom</Text>
                                </View>

                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>TV Kabel</Text>
                                </View>
                           </View>
                           <View style={[styles.topIcon]}>
                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>PDAM</Text>
                                </View>

                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>Pasca bayar</Text>
                                </View>

                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>Finance</Text>
                                </View>

                                <View style={{alignItems: 'center', width: '25%' }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name={Platform.OS === 'android' ? 'md-cube' : 'ios-cube'} size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>Kartu Kredit</Text>
                                </View>
                           </View>
                           <View style={[styles.topIcon]}>
                                <View style={{alignItems: 'center', width: '25%', marginBottom: 7 }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name='logo-game-controller-b' size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>PGN</Text>
                                </View>
                           </View>
                       </View>
                       {/* bottom third item */}
                       <View style={[styles.bottomItemInner, {marginTop: 10}]}>
                            <View style={{paddingLeft: 15, paddingTop: 5}}>
                               <Text style={{fontWeight: 'bold', fontSize: 13, color: 'black'}}>DONASI</Text>
                               <Text style={{fontSize: 10}}>Uluran tanganmu sangat berarti untuk mereka!</Text>
                            </View>
                            <View style={[styles.topIcon]}>
                                <View style={{alignItems: 'center', width: '25%', marginBottom: 7 }}>
                                    <View style={{borderWidth: 1, borderRadius: 10}}>
                                        <Icon style={{padding: 5}} name='logo-game-controller-b' size={35} color='green'/>
                                    </View>
                                    <Text style={{fontSize: 10}}>DONASI</Text>
                                </View>
                           </View>
                       </View>
                   </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },  
    top: {
        height: '45%'
        
    },
    topItem: {
        width: '100%',
        height: '50%',
        padding: 5
    },
    topItemInner: {
        flex: 1,
        backgroundColor: '#fff',
        elevation: 5,
        borderRadius: 10

    },
    topColor: {
        backgroundColor: 'green'
    },
    topIcon: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10
    },
    topIconSecond: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10,
        backgroundColor: '#32CD32',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    topTextUI: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    topText: {
        color: 'white',
        fontWeight: 'bold'
    },
    center: {
        height: '5%',
        backgroundColor: '#fff'
        
    },
    bottom: {
        height: '50%'
    },
    bottomItem: {
        width: '100%',
        height: '100%',
        padding: 5
    },
    bottomItemInner: {
        flex: 1,
        backgroundColor: '#fff',
        shadowColor: '#000',
        borderRadius: 5,
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 2
    }
});

export default Payment;