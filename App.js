import {Navigation} from 'react-native-navigation';
import { Provider } from 'react-redux';

import AuthScreen from './src/screens/Auth/Auth';
import SharePlaceScreen from './src/screens/SharePlace/SharePlace';
import FindPlaceScreen from './src/screens/FindPlace/FindPlace';
import PlaceDetailScreen from './src/screens/PlaceDetail/PlaceDetail';
import SideDrawer from './src/screens/SideDrawer/SideDrawer';
import configureStore from './src/store/configureStore';
import PaymentScreen from './src/screens/Payment/Payment';
import TestingScreen from './src/screens/Testing/Testing';
import TransferPaymentScreen from './src/screens/TransferPayment/TransferPayment';
import OtuIdPaymentScreen from './src/screens/OtuIdPayment/OtuIdPayment';
import RiwayatPaymentScreen from './src/screens/RiwayatPayment/RiwayatPayment';
import TopUpPaymentScreen from './src/screens/TopUpPayment/TopUpPayment';

const store = configureStore();

//Register Screen
Navigation.registerComponent("tes.AuthScreen", () => AuthScreen, store, Provider);
Navigation.registerComponent("tes.SharePlaceScreen", () => SharePlaceScreen, store, Provider);
Navigation.registerComponent("tes.FindPlaceScreen", () => FindPlaceScreen, store, Provider);
Navigation.registerComponent("tes.PlaceDetailScreen", () => PlaceDetailScreen, store, Provider);
Navigation.registerComponent("tes.SideDrawer", () => SideDrawer, store, Provider);
Navigation.registerComponent("tes.PaymentScreen", () => PaymentScreen);
Navigation.registerComponent("tes.TestingScreen", () => TestingScreen);
Navigation.registerComponent("tes.TransferPaymentScreen", () => TransferPaymentScreen);
Navigation.registerComponent("tes.OtuIdPaymentScreen", () => OtuIdPaymentScreen);
Navigation.registerComponent("tes.RiwayatPaymentScreen", () => RiwayatPaymentScreen);
Navigation.registerComponent("tes.TopUpPaymentScreen", () => TopUpPaymentScreen);
//Start a App
export default () => Navigation.startSingleScreenApp({
  screen: {
    screen: "tes.AuthScreen",
    title: "Login"
  }
});